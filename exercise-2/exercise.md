### **Brief**

1 - Answer these questions about the design of this [landing](https://logtail.com/)

a) What makes this a good design? What elements? What decisions?

    - In the header explain in one sentense what they offers: clean, clear, short. 
    - After that just a little bit of information and "Integrate in 5 minutes" convince people easily to "Let's try".
    - When the user scrolls not "get bored" because animation of elements fadeIn, etc. And something important is always appear
    a half of a new "chart" or "element" that makes one to ask "what's that??" and still scrolling down. 
    - Intermitents colors combined making clear the "change of topic" but still continue. 
    - All time showing contact.
    - All wrapped
    - Colors and FontFamily gives confidence.

b) How do you think was the process of the designers to get to this result?

    1) Get one and short idea to sell/show/offer.
    2) Know the client and know how they think, feel and why they will choose the product. 
    3) Choose Colors and font-family combined that give confidence.
    4) Choose a phrase and img to put in the header.
    5) Decide how many section will be. 
    6) Choose position of elements and animations to make the client scroll down while is convincing.
    7) All time show contact and should be easy to start.
    8) Then, start to build it.


2 - Using Figma, create a mood board about the landing. Represent these three aspects:

a) Color palette

b) Illustrations style (related in some way to the landing)

c) Typography (find one similar in Google fonts if the original is not free): Mulish

### **Important clarifications**

- These 2 points should take you less than an hour.
- The mood board should not be a big design project, it is just to demonstrate you have a well understanding of Figma as a tool. Think of it as a sketching task that should be done in around half an hour, it's shouldn't be pixel perfect.

Link to figma proyect: https://www.figma.com/file/jnWqSxnR9uCTuhJ6JNc4Gp/FigmaTest?node-id=0%3A1

### Steps

1 ) Fork the repository [https://bitbucket.org/wildaudience/hiring-exercises-2021/src/master/](https://bitbucket.org/wildaudience/hiring-exercises-2021/src/master/) (same as exercise 1)

2. Inside `exercise-2/exercise.md` write down the answers to point 1 and paste link to the figma project of point 2.

3. Commit the changes and push to master. Share the link to the repo and send an invite to the Figma project to `l@wildaudience.com`
