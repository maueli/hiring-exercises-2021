import React from 'react';
import { ApolloProvider } from '@apollo/client';
import { ApolloClient, InMemoryCache } from '@apollo/client';
import ListUsers from './components/ListUsers';
import Header from './components/Header';

const client = new ApolloClient({
  uri: 'https://graphqlzero.almansi.me/api',
  cache: new InMemoryCache()
});

// It was build with internal variable control. Of course could be better to create inputs to select such info or ranges.
function App() {
    return (
        <div className="App bg-gray-300 w-full h-full mt-0 p-12">
            <Header 
                title="User List in DataBase"
                subtitle="The database is graphqlzero. List of 10 users with their info."/>
            <ApolloProvider client={client}>
                <ListUsers 
                    rangeInit={1} 
                    rangeEnd={10} 
                    min={1} 
                    max={10} />
            </ApolloProvider>  
        </div>
    );
}

export default App;
