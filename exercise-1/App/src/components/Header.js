import React from "react";

const Header = (props)=>{
    return(
        <div className="max-w-sm w-full lg:max-w-full lg:flex">
            <div className="bg-white flex flex-col justify-between leading-normal m-auto p-12">
                <div className="mb-8">
                    <div className="text-gray-900 font-bold text-4xl mb-12">
                        {props.title}
                    </div>
                    <p className="text-gray-700 text-base">
                        {props.subtitle}
                    </p>
                </div>
            </div>
        </div>
    )
};

export default Header;


