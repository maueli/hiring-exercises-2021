import React, {useState, useEffect} from "react";
import ErrorComponent from "./ErrorComponent";
//import User from "./User";

import User from "./Item/User";

const ListUsers = (props)=>{
    const [idUsers, setIdUsers] = useState([]);
    const [error, setError] = useState(false);
    const {rangeInit, rangeEnd, min, max} = props;

    useEffect(()=>{
        // Avoid rangebers outside range
        if(rangeInit < min || rangeEnd > max){
            setError(true);
        }
    },[]);

    useEffect(()=>{
        // RangeSelected of Users
        const range = ()=>{
            let rangeSelected = [];
            for ( let i=rangeInit; i <= rangeEnd; i++ ) {
                rangeSelected.push(i);
            }
            return rangeSelected;
        }

        const range2 = range();
        setIdUsers( range2 );
    },[]);


    if(error) return <ErrorComponent/>

    // Info requested of users.
    const infoRequired = ["id", "username", "name", "email"];
    return(
        <>
            {idUsers.map((e,index)=>{
                return (
                    <User 
                        id={e} 
                        dataQuery={infoRequired}
                        key={index}/>
                )
            })}
        </>
    )
};

export default ListUsers;