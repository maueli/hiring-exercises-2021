import React from "react";

const HeaderItem = (props)=>{
    return(
        <div className="text-gray-900 font-bold text-lg mb-6"> 
            User Id: {props.id} 
        </div>
    )
};

export default HeaderItem;