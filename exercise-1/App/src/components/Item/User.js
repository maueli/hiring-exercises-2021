import React from 'react';
import { useQuery, gql } from '@apollo/client';
import Loading from '../Loading';
import ErrorComponent from '../ErrorComponent';
import CardLayout from '../CardLayout';

import HeaderItem from './HeaderItem';
import UserDataList from "./UserDataList";


const User = (props) => {
    const GET_USER = gql`
        {
            user(id:${props.id}) {
                ${props.dataQuery}
            }
        }`;
    const { loading, error, data } = useQuery(GET_USER);

    if (loading) return <Loading />;
    if (error) return <ErrorComponent error={error} />;




    return(
        <CardLayout>
            <>     
                <HeaderItem  id={data.user.id}/>
                <UserDataList dataQuery={props.dataQuery} dataUser={data.user} />
            </>
        </CardLayout>
        
    )
}


export default User;

