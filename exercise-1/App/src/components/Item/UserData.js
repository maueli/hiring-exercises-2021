import React from "react";

const UserData = (props)=>{
    return(
        <div className="text-gray-900 text-md mb-6">
            {props.title}: {props.info}
        </div>
    )
}

export default UserData;