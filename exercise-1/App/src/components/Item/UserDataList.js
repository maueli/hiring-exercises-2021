import React from "react";
import UserData from "./UserData";

const UserDataList = (props)=>{
    // Iterate of all items of the query info: i.e, name, email, etc.
    return props.dataQuery.map((e,index)=>{
        return(
            <UserData
                title={e}
                info = {props.dataUser[e]}
                key={index}/>
            )
        }
    )
};

export default UserDataList;